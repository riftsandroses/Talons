package funcs;

public class header {
    public static void asciiheader() {
        System.out.println("_________ _______  _        _______  _        _______        _______  _       _________");
        System.out.println("\\__   __/(  ___  )( \\      (  ___  )( (    /|(  ____ \\      (  ____ \\( \\      \\__   __/");
        System.out.println("   ) (   | (   ) || (      | (   ) ||  \\  ( || (    \\/      | (    \\/| (         ) (   ");
        System.out.println("   | |   | (___) || |      | |   | ||   \\ | || (_____       | |      | |         | |   ");
        System.out.println("   | |   |  ___  || |      | |   | || (\\ \\) |(_____  )      | |      | |         | |   ");
        System.out.println("   | |   | (   ) || |      | |   | || | \\   |      ) |      | |      | |         | |   ");
        System.out.println("   | |   | )   ( || (____/\\| (___) || )  \\  |/\\____) |      | (____/\\| (____/\\___) (___");
        System.out.println("   )_(   |/     \\|(_______/(_______)|/    )_)\\_______)      (_______/(_______/\\_______/");
        System.out.println("                                                                 Made by @riftsandroses");
    }
}
