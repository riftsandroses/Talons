package funcs;

public class display_menu {
    public static void display() {
        System.out.println("\n1. Display detailed Animal List.");
        System.out.println("0. Exit Talons CLI");
    }
    public static void list() {
        System.out.println("\nEnter the corresponding number for the Animal - ");
        sleep.pause(1000);
        System.out.println("1. Acorn Barnacle");
        System.out.println("2. American Black Sturgeon");
        System.out.println("3. American Black Tick");
        System.out.println("4. American Lobster");
        System.out.println("5. Ascaris");
        System.out.println("6. Blue Poison Dart Frog");
        System.out.println("7. Blue Ringed Octopus");
        System.out.println("8. Box Jellyfish");
        System.out.println("9. Brittle Worm");
        System.out.println("10. Carpet Anemone");
        System.out.println("11. Centipede");
        System.out.println("12. Dungeness Crab");
        System.out.println("13. Earth Worm");
        System.out.println("14. Filaria");
        System.out.println("15. Garden Snail");
        System.out.println("16. German Cockroach");
        System.out.println("17. German Shepherd");
        System.out.println("18. Great White Shark");
        System.out.println("19. Hagfish");
        System.out.println("20. Hook Worm");
        System.out.println("21. Horshoe Crab");
        System.out.println("22. House Spider");
        System.out.println("23. Lake Trout");
        System.out.println("24. Leech");
        System.out.println("25. Lugworm");
        System.out.println("26. Millipede");
        System.out.println("27. Monitor Lizard");
        System.out.println("28. Pacific Oyster");
        System.out.println("29. Passenger Pigeon");
        System.out.println("30. Pin Worm");
        System.out.println("31. Rock Shrimp");
        System.out.println("32. Sand Dollar");
        System.out.println("33. Sea Urchin");
        System.out.println("34. Sponge");
        System.out.println("35. Starfish");
        System.out.println("36. Striped Bark Scorpion");
        System.out.println("37. Tape Worm");
        System.out.println("38. Whip Worm");
        System.out.println("0. Exit");
        System.out.print("\nEnter your choice: ");
    }
}