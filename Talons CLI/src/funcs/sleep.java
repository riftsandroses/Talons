package funcs;
import java.lang.Thread;
public class sleep {
    public static void pause(int n) {
        try {
            Thread.sleep(n);
        }
        catch(Exception e) {
            System.out.println(e + " Detected.");
        }
    }
}
