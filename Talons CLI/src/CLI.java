import java.util.Scanner;
import animalia.arthropoda.chelicerates.arachnids.*;
import animalia.arthropoda.crustaceans.malacostracans.*;
import animalia.arthropoda.crustaceans.maxillopoda.*;
import animalia.chordata.vertebrata.osteichthyes.*;
import animalia.nematoda.*;
import animalia.chordata.vertebrata.amphibian.*;
import funcs.*;

public class CLI {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int choice, option;
        float age;
        header.asciiheader();
        sleep.pause(1000);
        while (true) {
            display_menu.display();
            System.out.print("Enter your choice: ");
            choice = sc.nextInt();
            if (choice == 0) {
                break;
            }
            else if (choice == 1) {
                display_menu.list();
                System.out.print("Enter your choice: ");
                option = sc.nextInt();
                System.out.print("Enter the human age: ");
                age = sc.nextFloat();
                switch (option) {
                    case 1: System.out.println("The subsequent age of Acorn Barnacle will be: " + acornbarnacle.ageAcornbaracle(age));
                    case 2: System.out.println("The subsequent age of American Black Sturgeon will be: " + americanblacksturgeon.ageAmericanblacksturgeon(age));
                    case 3: System.out.println("The subsequent age of American Dog Tick will be: " + americandogtick.ageAmericandogtick(age));
                    case 4: System.out.println("The subsequent age of American Lobster will be: " + americanlobster.ageAmericanlobster(age));
                    case 5: System.out.println("The subsequent age of Ascaris will be: " + ascaris.ageAscaris(age));
                    case 6: System.out.println("The subsequent age of Blue Poison Dart Frog will be: " + bluepoisondartfrog.ageBluepoisondartfrog(age));
                    case 7: System.out.println("The subsequent age of Blue Ringed Octopus will be: ");
                }
            }
            else {
                System.out.println("\nInvalid Input. Please try again.");
                sleep.pause(1000);
                }
            }
        }
    }